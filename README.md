# Filter #

This class enables you to easily validate and sanitize data using filter definitions.

The filter definitions can be combined to perform a series of validation/ sanitization operations.

##Basic Validation Usage##

```
Use dgifford\Filter\Validator;

// Valid email
$email = new Validator( 'dude@example.com', 'email' );

var_dump( $email->isValid() ); // true

$email->setValue( 'dude@@@@example.com' );

var_dump( $email->isValid() ); // false
```

##Basic Sanitization Usage##

```
Use dgifford\Filter\Sanitizer;

$email = new Sanitizer( ' 	dude@example.com ', 'email' );

echo $email->value;  // ' 	dude@example.com '
echo $email->result; // 'dude@example.com'

var_dump( $email->isValid() ); // true

```

##Editing values and filters##

```
Use dgifford\Filter\Validator;

$email = new Validator( 'dude@example.com', 'email' );

$email->setValue( 'wibble@example.com' );

$email->setFilter( 'email_rfc2822' );

$email->set( 'wibble@example.com', 'email_rfc2822' );

```

##Combining filters##

Filters are combined by seperating them with a space.
```
Use dgifford\Filter\Sanitizer;

$email = new Sanitizer( ' 	dude@example.com ', 'trim email' );

echo $email->result; // 'dude@example.com'
```

##Filter Variables##

Some filters can contain variables. This are concatenated to the end of the filter name with the pipe symbol, | .
```
$email = new Validator( 'dude@example.com', 'email ends_with|@example.com' );

var_dump( $email->isValid() ); // true
```