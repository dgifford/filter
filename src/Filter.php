<?php
Namespace dgifford\Filter;

/*
	Abstract class requiring extending classes to create a 'process' 
	method and defining methods for setting filter arguments.


    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

Use dgifford\Traits\ArrayHelpersTrait;
Use dgifford\Filter\Sanitizer;
Use dgifford\Filter\Validator;



abstract class Filter
{
	Use ArrayHelpersTrait;

	// Original value
	public $value;

	// Filters to apply
	public $filters = [];

	// Seperates filter from variable
	public $filter_separator = '|';

	// Result of processiong
	public $result;

	// Prefix for validation and sanitization functions
	public $prefix = '_';





	/**
	 * Constructor. Can pass arguments in constructor.
	 * Calls set method.
	 */
	public function __construct()
	{
		// Chainable
		return call_user_func_array( [$this, 'set' ], func_get_args() );
	}



	/**
	 * Process filters, then return result.
	 * 
	 * @return mixed
	 */
	public function result()
	{
		$this->process();

		return $this->result;
	}



	/**
	 * Method in extending classes that performs validation/ sanitisation
	 */
	abstract protected  function process();



	/**
	 * Cleares the result properties
	 * @return null
	 */
	public function clearResult()
	{
		$this->result = null;

		// Chainable
		return $this;
	}



	/**
	 * Set the value and, optionally, the filter type.
	 */
	public function set()
	{
		$args = func_get_args();

		// First argument is the value to be filtered
		if( isset($args[0]) )
		{
			$this->setValue( $args[0] );
		}

		// Second argument is a string or array of filters
		if( isset($args[1]) and (is_string($args[1]) or is_array($args[1]) ))
		{
			$this->setFilters( $args[1] );
		}

		// Chainable
		return $this;
	}



	/**
	 * Adds to the filters used.
	 * @param string $filters String of Filter definitions
	 */
	public function addFilters( $filters = '' )
	{
		if( is_string( $filters ) )
		{
			$filters = $this->stringToArray( $filters, true, true, [',',' ',"\n"] );
		}

		if( is_array( $filters ) )
		{
			foreach( $filters as $filter )
			{
				if( is_string( $filter ) )
				{
					$split = explode( $this->filter_separator, $filter);

					if( !isset($split[1]) )
					{
						$split[1] = '';
					}

					$this->filters[] = ['filter' => $split[0], 'value' => $split[1] ];
				}
				elseif( is_array( $filter ) )
				{
					$this->filters[] = $filter;
				}
			}
		}

		// Chainable
		return $this;
	}



	/**
	 * Sets the filters used. [description]
	 */
	public function setFilters( $filters = '' )
	{
		$this->filters = [];
		
		$this->addFilters( $filters );

		// Chainable
		return $this;
	}



	/**
	 * Sets filters based on form control attributes.
	 * @param array $attributes
	 */
	public function setFiltersFromAttributes( $attributes = [] )
	{
		$filters = [];

		$sanitizer = new Sanitizer;
		$validator = new Validator;

		foreach( $attributes as $name => $value )
		{
			switch( $name )
			{
				case 'type':
					$method = $this->prefix . $value;

					if( 
						method_exists( $sanitizer, $method )
						or
						method_exists( $validator, $method )
					)
					{
						$filters[] = $value;
					}

					if( $value == 'text' )
					{
						$filters[] = 'string';
					}
				break;


				case 'step':
					$filters[] = 'divisible' . $this->filter_separator . $value;
				break;

			
				default:
					$method = $this->prefix . $name;

					if( 
						method_exists( $sanitizer, $method )
						or
						method_exists( $validator, $method )
					)
					{
						$filters[] = $name . $this->filter_separator . $value;
					}
				break;
			}
		}

		$this->setFilters( $filters );

		// Chainable
		return $this;
	}



	/**
	 * Set the value to be filtered.
	 * @param mixed $value 
	 */
	public function setValue( $value = null )
	{
		$this->value = $value;

		// Chainable
		return $this;
	}



	/**
	 * Parse the arguments provided and populate the object's properties
	 * @param  array  $args An array of arguments passed to the function
	 */
	protected function parseArguments( $args = array() )
	{
	}





	//////////////////////////////////////////////////////////////////
	// Static Methods
	//////////////////////////////////////////////////////////////////



	/**
	 * Return a default value if the sanitized value is not valid.
	 * 
	 * @param  mixed $value
	 * @param  string $filters
	 * @param  string $default
	 * @return mixed
	 */
	public static function default( $value, $filters = '', $default = '' )
	{
		$sanitizer = new Sanitizer( $value, $filters );

		if( $sanitizer->isValid() )
		{
			return $sanitizer->result();
		}
		else
		{
			return $default;
		}
	}



	public static function validate( $value, $filters = '' )
	{
		$validator = new Validator( $value, $filters );

		return $validator->isValid();
	}



	public static function sanitize( $value, $filters = '' )
	{
		$sanitizer = new Sanitizer( $value, $filters );

		return $sanitizer->result();
	}

}