<?php
Namespace dgifford\Filter;



/*
	Sanitizer extends the Filter class and sanitizes a value using
	defined filters. The result is stored in the 'result' property.


    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



Use dgifford\Traits\MethodCallingTrait;
Use Alcohol\ISO4217;



class Sanitizer extends Filter
{
	Use MethodCallingTrait;





	/**
	 * Process filters as sanitizers.
	 * 
	 * @return null
	 */
	protected function process()
	{
		$this->result = $this->value;

		foreach( $this->filters as $arr )
		{
			// Call sanitization method
			$this->callIfMethodExists( $this->prefix . $arr['filter'], $arr['value'], $this->result );
		}
	}



	/**
	 * Boolean test if santized value is valid
	 * @return boolean [description]
	 */
	public function isValid()
	{
		$validator = new Validator( $this->result(), $this->filters );

		return $validator->isValid();
	}



	/**
	 * Perform preg replace on result.
	 * @param  string  $pattern     Regex
	 * @param  string  $replacement Replacement string
	 * @param  integer $limit
	 * @return null
	 */
	protected function preg_replace( $pattern , $replacement , $limit = -1 )
	{
		$this->result = preg_replace( $pattern, $replacement, $this->result, $limit, $this->preg_replace_count );
	}







	/////////////////////////////////////////
	// The Sanitization methods
	/////////////////////////////////////////



	/**
	 * Validates an email using PHP's standard FILTER_VALIDATE_EMAIL
	 * Replaces spaces with _ , trims and lowercase
	 * @return [type] [description]
	 */
	protected function _email()
	{
		$this->result = filter_var( $this->result, FILTER_SANITIZE_EMAIL);
	}


	
	protected function _special_chars()
	{
	    $this->result = htmlspecialchars( $this->result, ENT_COMPAT | ENT_XHTML, $this->charset );
	}



	protected function _string()
	{
		$this->result = filter_var( $this->result, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH );
	}



	/**
	 * Sanitizes a string by trimming whitespace or a custom character mask
	 * @return [type] [description]
	 */
	protected function _trim( $value = '' )
	{
		if( !empty( $value ) )
		{
			$this->result = trim( $this->result, $value );
		}
		else
		{
			$this->result = trim( $this->result );
		}
	}



	/**
	 * Sanitizes a string by trimming whitespace or a custom character mask
	 * @return [type] [description]
	 */
	protected function _rtrim( $value = '' )
	{
		if( !empty( $value ) )
		{
			$this->result = rtrim( $this->result, $value );
		}
		else
		{
			$this->result = rtrim( $this->result );
		}
	}



	/**
	 * Sanitizes a string by trimming whitespace or a custom character mask
	 * @return [type] [description]
	 */
	protected function _ltrim( $value = '' )
	{
		if( !empty( $value ) )
		{
			$this->result = ltrim( $this->result, $value );
		}
		else
		{
			$this->result = ltrim( $this->result );
		}
	}



	/**
	 * @return [type] [description]
	 */
	protected function _uppercase()
	{
		$this->result = strtoupper( $this->result );
	}



	/**
	 * @return [type] [description]
	 */
	protected function _lowercase()
	{
		$this->result = strtolower( $this->result );
	}



	/**
	 * @return [type] [description]
	 */
	protected function _titlecase()
	{
		$this->result = ucwords( $this->result );
	}



	/**
	 * @return [type] [description]
	 */
	protected function _sentencecase()
	{
		$this->result = ucfirst( $this->result );
	}



	/**
	 * Removes all non-alphanumeric characters, except -_:
	 * Replaces spaces with _ and trims whitespace
	 * Must start with a - z or A - Z
	 * This regex uses the more restrictive HTML4 definition.
	 * https://www.w3.org/TR/html4/types.html#type-id
	 * 
	 * HTML5 is less restrictive: https://www.w3.org/TR/html5/dom.html#the-id-attribute
	 * @return [type] [description]
	 */
	protected function _name_attribute()
	{
		// trim and convert to lowercase
		$this->_trim();
		//$this->_lowercase();

		// Remove unwanted characters and replace spaces
		$this->preg_replace( array( "/^[^a-zA-Z]*/", "/\\s+/", "/[^a-z0-9\\-\\_\\|\\:]*/i",  ), array( "","_", "", ) , $limit = -1 );
	}



	/**
	 * Same as name attribute
	 * 
	 * @return bool
	 */
	protected function _id_attribute()
	{
		return $this->_name_attribute();
	}



	/**
	 * Sanitizes a a string into snake case
	 * @return [type] [description]
	 */
	public function _snake_case()
	{
		$this->_trim();

		$this->preg_replace( array( "/\\s+/", "/[^a-z0-9\\-\\_]*/i", ), array( "_", "",) , $limit = -1 );
	}



	/**
	 * Sanitizes a a string into camel case
	 * https://en.wikipedia.org/wiki/CamelCase
	 * @return [type] [description]
	 */
	public function _camel_case()
	{
		$this->_trim();
		$this->_titlecase();

		$this->preg_replace( array( "/\\s+/", "/[^a-z0-9\\-\\_]*/i",) , array( "",) , $limit = -1 );
	}



	/**
	 *  @brief
	 *  Sanitizes a filename, replacing whitespace with dashes.
	 *  Based on wordpress function, http://codex.wordpress.org/Function_Reference/_file_name
	 *  
	 *  @param string $filename The filename to be sanitized
	 *  @return string The sanitized filename
	 *  
	 *  @details
	 *  Removes special characters that are illegal in filenames on certain
	 *  operating systems and special characters requiring special escaping
	 *  to manipulate at the command line. Replaces spaces and consecutive
	 *  dashes with a single dash. Trims period, dash and underscore from beginning
	 *  and end of filename.
	 */
	public function _filename()
	{
		$special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", chr(0));

		$this->result = preg_replace( "#\x{00a0}#siu", ' ', $this->result );
		$this->result = str_replace($special_chars, '', $this->result);
		$this->result = str_replace( array( '%20', '+' ), '-', $this->result );
		$this->result = preg_replace('/[\s-]+/', '-', $this->result);
		$this->result = trim($this->result, '.-_');
	}



	/**
	 * Sanitizes a a string into camel case
	 * https://en.wikipedia.org/wiki/CamelCase
	 * @return [type] [description]
	 */
	public function _boolean()
	{
		$this->result = boolval( $this->result );
	}



	/**
	 * @return [type] [description]
	 */
	public function _max( $value )
	{
		if( $this->result > (0 + $value) )
		{
			$this->result = $value;
		}
	}



	/**
	 * @return [type] [description]
	 */
	public function _min( $value )
	{
		if( $this->result < (0 + $value) )
		{
			$this->result = $value;
		}
	}



	/**
	 * @return [type] [description]
	 */
	public function _divisible( $value )
	{
		if( fmod($this->result, $value) != 0 )
		{
			$this->result += 6 - ($this->result % 6);
		}
	}



	/**
	 * @return [type] [description]
	 */
	public function _maxlength( $value )
	{
		$value = 0 + $value;
		$this->result = substr( $this->result, 0, $value);
	}



	/**
	 * @return [type] [description]
	 */
	public function _strip_tags()
	{
		$this->result = strip_tags($this->result);
	}



	/**
	 * @return [type] [description]
	 */
	public function _maxwords( $value )
	{
		$value = 0 + $value;

		$words = explode(' ', $this->result);

		$arr = array_chunk($words, $value);

		$this->result = implode(' ', $arr[0]);
	}



	/**
	 * @return [type] [description]
	 */
	public function _realpath()
	{
		$this->result = realpath( $this->result );
	}



	protected function _number()
	{
		$this->result = 0 + filter_var( $this->result, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION|FILTER_FLAG_ALLOW_THOUSAND|FILTER_FLAG_ALLOW_SCIENTIFIC);
	}


	/**
	 * Only allow +-. and digits
	 * @return [type] [description]
	 */
	protected function _strict_number()
	{
		$this->result = 0 + filter_var( $this->result, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
	}



	protected function _int()
	{
		$this->result = 0 + filter_var( $this->result, FILTER_SANITIZE_NUMBER_INT );
	}



	protected function _positive()
	{
		$this->result = abs( $this->result );
	}



	protected function _float()
	{
		$float = filter_var( $this->result, FILTER_VALIDATE_FLOAT);

		if( $float === false )
		{
			$this->result = null;
		}
		else
		{
			$this->result = $float;
		}
	}



	protected function _ip()
	{
		$ip = filter_var( $this->result, FILTER_VALIDATE_IP);

		if( $ip === false )
		{
			$this->result = null;
		}
		else
		{
			$this->result = $ip;
		}
	}



	/**
	 * Returns a string as a number of bytes, converting 1G, 1M, 1K etc.
	 * If the result is not an integer, 0 is returned.
	 * See: http://php.net/manual/en/function.ini-get.php
	 * 
	 * @param  [type] $value [description]
	 * @return [type]        [description]
	 */
	public function _bytes()
	{
		$last_char = strtolower( substr($this->result, -1) );

		switch($last_char) 
		{
			// The 'G' modifier is available since PHP 5.1.0
			case 'g':
			$this->result *= 1024;
			case 'm':
			$this->result *= 1024;
			case 'k':
			$this->result *= 1024;
		}

		$this->result = intval( $this->result );
   	}



   	public function _currency_code()
   	{
   		$this->result = strtoupper( substr(trim($this->result), 0, 3 ) );
   	}
}