<?php
Namespace dgifford\Filter;



/*
	Validator extends the Filter class and validates a value against
	the defined filters. The result of each validation is stored in the
	'result' array. The isValid() method returns false if any of the
	validation operations returns false.


    Copyright (C) 2016  Dan Gifford

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



Use dgifford\Traits\MethodCallingTrait;
Use Alcohol\ISO4217;



class Validator extends Filter
{
	Use MethodCallingTrait;





	/**
	 * Process validation filters
	 * @return null
	 */
	protected function process()
	{
		$this->result = [];

		foreach( $this->filters as $filter_array )
		{
			// Call validation method
			$this->result[ $this->getFilterKey( $filter_array ) ] = $this->callIfMethodExists( $this->prefix . $filter_array['filter'], $filter_array['value'], null );
		}
	}



	/**
	 * Return the index of the filters used in the results array.
	 * 
	 * @param  array  $filter_array
	 * @return string
	 */
	protected function getFilterKey( $filter_array = [] )
	{
		return trim( $filter_array['filter'] . $this->filter_separator . $filter_array['value'], $this->filter_separator );
	}



	/**
	 * Returns false if any of the filters results in false.
	 * 
	 * @return boolean
	 */
	public function isValid()
	{
		$this->process();

		foreach( $this->result as $value )
		{
			if( $value === false )
			{
				return false;
			}
		}

		return true;
	}



	/**
	 * Performs a preg match on value.
	 * @param  string $regex [description]
	 * @return null
	 */
	protected function preg_match( $regex )
	{
		$this->preg_match_result = preg_match( $regex , $this->value, $this->preg_match_matches );
	}






	/////////////////////////////////////////
	// General validation methods
	/////////////////////////////////////////



	/**
	 * Tests if value is empty.
	 * @return bool
	 */
	public function _empty()
	{
		return empty( $this->value );
	}



	/**
	 * @return [type] [description]
	 */
	public function _equal_to( $value )
	{
		if( $this->value == $value )
		{
			return true;
		}

		return false;
	}






	//////////////////////////////////////////////////
	// Strings
	//////////////////////////////////////////////////
	


	/**
	 * Validates an email using PHP's standard FILTER_VALIDATE_EMAIL
	 * @return bool
	 */
	protected function _is_string()
	{
		return is_string( $this->value );
	}
	


	/**
	 * Validates an email using PHP's standard FILTER_VALIDATE_EMAIL
	 * @return bool
	 */
	protected function _email()
	{
		if( $this->value === filter_var( $this->value, FILTER_VALIDATE_EMAIL ) )
		{
			return true;
		}

		return false;
	}



	/**
	 * Validates an email to rfc3696
	 * @return bool
	 */
	protected function _email_rfc3696()
	{
		require_once 'rfc3696-email-parser.php';

		return boolval( is_rfc3696_valid_email_address( $this->value ) );		
	}



	/**
	 * Validates an email to rfc2822
	 * @return bool
	 */
	protected function _email_rfc2822()
	{
		require_once 'rfc3696-email-parser.php';

		return boolval( is_rfc2822_valid_email_address( $this->value ) );
	}



	/**
	 * Validates an email using email validator
	 * @return bool
	 */
	protected function _email_validator()
	{
		$validator = new EmailValidator();

		return $validator->isValid( $this->value, new RFCValidation() );
	}



	/**
	 * Validates an email using a DNS lookup
	 * @return bool
	 */
	protected function _email_validator_dns()
	{
		$validator = new EmailValidator();

		return $validator->isValid( $this->value, 
			new MultipleValidationWithAnd([
				new RFCValidation(),
				new DNSCheckValidation()
			]) 
		);
	}



	/**
	 * Removes all non-alphanumeric characters, except -_:
	 * Replaces spaces with _ and trims whitespace
	 * Must start with a - z or A - Z
	 * This regex uses the more restrictive HTML4 definition.
	 * https://www.w3.org/TR/html4/types.html#type-id
	 * 
	 * HTML5 is less restrictive: https://www.w3.org/TR/html5/dom.html#the-id-attribute
	 * @return [bool] [description]
	 */
	protected function _name_attribute()
	{
		$this->preg_match( "/^[a-z][a-z0-9\\-\\_\\|\\:]*$/i" );

		if( $this->preg_match_result === 1 )
		{
			return true;
		}
		
		return false;
	}



	/**
	 * Same as name attribute
	 * @return bool
	 */
	protected function _id_attribute()
	{
		return $this->name_attribute();
	}



	/**
	 * Hex RGB color value, e.g. #00FF00
	 * 
	 * @return bool
	 */
	protected function _hex_rgb_colour()
	{
		$this->preg_match( "/#[0-9a-f]{6}/i" );

		if( $this->preg_match_result === 1 )
		{
			return true;
		}
		
		return false;
	}



	/**
	 * Tests if value is a URL.
	 * @return bool
	 */
	public function _url()
	{
		return filter_var( $this->value, FILTER_VALIDATE_URL);
	}



	/**
	 * Tests if ends with a string.
	 * @return bool
	 */
	public function _ends_with( $value )
	{
		if( substr( $this->value, (-1*strlen($value)) ) == $value )
		{
			return true;
		}

		return false;
	}



	/**
	 * @return [type] [description]
	 */
	public function _starts_with( $value )
	{
		if( substr( $this->value, 0, strlen($value)) == $value )
		{
			return true;
		}

		return false;
	}



	/**
	 * @return [type] [description]
	 */
	public function _maxlength( $value )
	{
		$value = 0 + $value;

		if( strlen( $this->value ) <= $value )
		{
			return true;
		}

		return false;
	}



	/**
	 * @return [type] [description]
	 */
	public function _minlength( $value )
	{
		if( strlen( $this->value ) >= $value )
		{
			return true;
		}

		return false;
	}



	/**
	 * @return [type] [description]
	 */
	public function _length( $value )
	{
		if( strlen( $this->value ) == $value )
		{
			return true;
		}

		return false;
	}


	protected function _ip()
	{
		if( $this->value === filter_var( $this->value, FILTER_VALIDATE_IP) )
		{
			return true;
		}
		
		return false;
	}



	protected function _visible_chars()
	{
		return ctype_graph( $this->value );
	}



	public function _currency_code()
	{ 		
		$iso4217 = new ISO4217();

		try
		{
			$result = $iso4217->getByAlpha3( $this->value );
		}
		catch( \DomainException $e)
		{
			return false;
		}
  		
  		return true;
	}






	//////////////////////////////////////////////////
	// Numbers
	//////////////////////////////////////////////////



	/**
	 * @return [type] [description]
	 */
	public function _greater_than( $value )
	{
		if( $this->value > $value )
		{
			return true;
		}

		return false;
	}


	/**
	 * @return [type] [description]
	 */
	public function _less_than( $value )
	{
		if( $this->value < $value )
		{
			return true;
		}

		return false;
	}



	/**
	 * @return [type] [description]
	 */
	public function _numeric()
	{
		if( is_numeric( $this->value ) )
		{
			return true;
		}

		return false;
	}



	/**
	 * Synonym for _numeric
	 * 
	 * @return [type] [description]
	 */
	public function _number()
	{
		return $this->_numeric();
	}



	/**
	 * Tests for numeric and correct type - not a string
	 * 
	 * @return [type] [description]
	 */
	public function _number_strict()
	{
		if( $this->_numeric() === true and !is_string( $this->value ) )
		{
			return true;
		}
		
		return false;
	}



	/**
	 * @return [type] [description]
	 */
	public function _min( $value )
	{
		if( $this->value < (0 + $value) )
		{
			return false;
		}

		return true;
	}



	/**
	 * @return [type] [description]
	 */
	public function _max( $value )
	{
		if( $this->value > (0 + $value) )
		{
			return false;
		}

		return true;
	}



	/**
	 * @return [type] [description]
	 */
	public function _divisible( $value )
	{
		if( ($this->value/$value - floor($this->value/$value)) != 0 )
		{
			return false;
		}

		return true;
	}



	/**
	 * Returns true if the value has the given number of
	 * decimal places
	 * @return bool
	 */
	protected function _decimal_places( $value )
	{
		if( $this->value = number_format( $this->value, $value ) )
		{
			return true;
		}
		
		return false;
	}



	/**
	 * @return [type] [description]
	 */
	public function _int()
	{
		return $this->integer();
	}



	public function _integer()
	{
		$result = filter_var( $this->value, FILTER_VALIDATE_INT );

		if( $result == $this->value and !is_bool( $this->value ) and !is_bool( $this->value) )
		{
			return true;
		}

		return false;
	}



	public function _positive()
	{
		if( $this->value > 0 )
		{
			return true;
		}

		return false;
	}



	public function _negative()
	{
		if( $this->value < 0 )
		{
			return true;
		}

		return false;
	}



	/**
	 * Returns true if the value is float between 0 an 1
	 * @return bool
	 */
	public function _rate()
	{
		if( is_float( $this->value ) and $this->value >= 0 and $this->value <= 1 )
		{
			return true;
		}

		return false;
	}




	protected function _float()
	{
		if( $this->value === filter_var( $this->value, FILTER_VALIDATE_FLOAT) )
		{
			return true;
		}
		
		return false;
	}






	//////////////////////////////////////////////////
	// File system
	//////////////////////////////////////////////////



	/**
	 * Returns true if the value is a path to a file
	 * @return bool
	 */
	public function _is_file()
	{
		return is_file( $this->value );
	}



	/**
	 * Returns true if the value is a path to a directory
	 * @return bool
	 */
	public function _is_dir()
	{
		return is_dir( $this->value );
	}


	/**
	 * Returns true if the value is a path or files that
	 * exists
	 * @return bool
	 */
	public function _file_exists()
	{
		return file_exists( $this->value );
	}



	/**
	 * Returns true if the value is a path that resolves
	 * to a real (existing) path
	 * @return bool
	 */
	public function _realpath()
	{
		if( realpath($this->value) === false )
		{
			return false;
		}

		return true;
	}
}