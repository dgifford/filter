<?php
Namespace dgifford\Filter;

Use dgifford\Filter\Sanitizer;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class SanitizerTest extends \PHPUnit_Framework_TestCase
{
	public function testArgumentsInConstructor()
	{
		$email = new Sanitizer( 'dude@example.com ', 'email' );

		$this->assertSame( 'dude@example.com', $email->result() );
	}



	public function testIfSanitizedIsValid()
	{
		$email = new Sanitizer( ' 	dude@example.com ', 'email' );

		$this->assertTrue( $email->isValid() );
	}



	public function testIfSanitizedIsInvalid()
	{
		$email = new Sanitizer( ' 	dude@@@example.com ', 'email' );

		$this->assertFalse( $email->isValid() );
	}



	public function testFilterDoesNotExist()
	{
		$email = new Sanitizer( 'dude@example.com', 'wibble' );

		$this->assertSame( 'dude@example.com', $email->result() );
	}



	public function testFilterExistsAndFilterDoesNotExist()
	{
		$email = new Sanitizer( 'dude@example.com   ', 'email ends_with|@example.com' );

		$this->assertSame( 'dude@example.com', $email->result() );
		$this->assertTrue( $email->isValid() );
	}



	public function testAddFiltersChangeValueAndResetFilters()
	{
		$email = new Sanitizer( 'dude@example.com   ', 'email' );

		$email->addFilters('ends_with|@example.com');

		$this->assertSame( 'dude@example.com', $email->result() );
		$this->assertTrue( $email->isValid() );

		$email->setValue('myemail@email.com   ');

		$this->assertSame( 'myemail@email.com', $email->result() );

		$this->assertFalse( $email->isValid() );

		$email->setFilters('email');

		$this->assertTrue( $email->isValid() );
	}



	public function testSetFiltersFromFormAttributes()
	{
		$number = new Sanitizer( '3' );

		$number->setFiltersFromAttributes([
			'type' => 'number',
			'max' => 4,
			'min' => 1,
			'step' => 1,
		]);

		$this->assertSame([
			[
				'filter' => 'number',
				'value'  => '',
			],
			[
				'filter' => 'max',
				'value'  => '4',
			],
			[
				'filter' => 'min',
				'value'  => '1',
			],
			[
				'filter' => 'divisible',
				'value' => '1',
			],
		]
		, $number->filters );
	}



	public function testDivisible()
	{
		$number = new Sanitizer( '3', 'divisible|0.5' );

		$this->assertTrue( $number->isValid() );

		$number->setFilters( 'divisible|1' );

		$this->assertTrue( $number->isValid() );

		$number->setFilters( 'divisible|1.5' );

		$this->assertTrue( $number->isValid() );

		$number->setFilters( 'divisible|0.9' );

		$this->assertFalse( $number->isValid() );
	}



	public function testChainConstructor()
	{
		$this->assertTrue( (new Sanitizer( '3', 'divisible|0.5' ))->isValid() );
	}



	public function testStaticMethod()
	{
		$this->assertSame( 'dude@example.com', Filter::sanitize( 'dude@example.com   ', 'email' ) );
	}



	public function testStaticDefaultMethod()
	{
		$this->assertSame( 'dude@example.com', Filter::default( 'dude@example.com   ', 'email' ) );

		$this->assertSame( 'dude@example.com', Filter::default( 'sdfsdf   ', 'email', 'dude@example.com' ) );
	}

}