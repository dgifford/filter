<?php
Namespace dgifford\Filter;

Use dgifford\Filter\Sanitizer;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class StaticMethodTest extends \PHPUnit_Framework_TestCase
{
	public function testStaticValidateMethod()
	{
		$this->assertTrue( Filter::validate( 'dude@example.com', 'email ends_with|@example.com' ) );
	}



	public function testStaticSanitizeMethod()
	{
		$this->assertSame( 'dude@example.com', Filter::sanitize( 'dude@example.com   ', 'email' ) );
	}



	public function testStaticDefaultMethod()
	{
		$this->assertSame( 'dude@example.com', Filter::default( 'dude@example.com   ', 'email' ) );

		$this->assertSame( 'dude@example.com', Filter::default( 'sdfsdf   ', 'email', 'dude@example.com' ) );
	}

}