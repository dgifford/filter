<?php
Namespace dgifford\Filter;

Use dgifford\Filter\Validator;
Use DMG\hf\hf;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class ValidatorTest extends \PHPUnit_Framework_TestCase
{
	public function testValidateInConstructor()
	{
		$val = new Validator( 'dude@example.com', 'email' );

		$this->assertTrue( $val->isValid() );
	}



	public function testChangeValueUsingSet()
	{
		$val = new Validator( 'dude@example.com', 'email' );

		$val->set( 'arse@example.com' );

		$this->assertSame( 'arse@example.com', $val->value );
	}



	public function testChangeValueUsingSetValue()
	{
		$val = new Validator( 'dude@example.com', 'email' );

		$val->setValue( 'arse@example.com' );

		$this->assertSame( 'arse@example.com', $val->value );
	}



	public function testChangeValueAndFilters()
	{
		$val = new Validator( 'dude@example.com', 'email' );

		$val->set( 'arse@example.com', 'email_rfc3696' );

		$this->assertSame( 'arse@example.com', $val->value );
		$this->assertTrue( $val->isValid() );
	}



	public function testChangeFiltersUsingSetFilters()
	{
		$val = new Validator( 'dude_name', 'email' );

		$val->setFilters( 'name_attribute' );

		$this->assertTrue( $val->isValid() );
	}



	public function testInvalidNameAttribute()
	{
		$name_attribute = new Validator( 'invalid name attr', 'name_attribute' );

		$this->assertFalse( $name_attribute->isValid() );
	}



	public function testEndsWith()
	{
		$val = new Validator( 'arse@example.com', 'ends_with|@example.com' );

		$this->assertTrue( $val->isValid() );
	}



	public function testCombiningFilters()
	{
		$val = new Validator( 'arse@example.com', 'email ends_with|@example.com' );

		$this->assertTrue( $val->isValid() );
	}



	public function testFailWhenCombiningFilters()
	{
		$val = new Validator( 'arse@ @example.com', 'email ends_with|@example.com' );

		$this->assertSame( [ 'email' => false, 'ends_with|@example.com' => true ], $val->result() );

		$this->assertFalse( $val->isValid() );
	}



	public function testHexRgbColour()
	{
		$val = new Validator( '#00ff00', 'hex_rgb_colour' );

		$this->assertTrue( $val->isValid() );
	}



	public function testIsDir()
	{
		$val = new Validator( __DIR__, 'is_dir' );

		$this->assertTrue( $val->isValid() );
	}



	public function testIsFile()
	{
		$val = new Validator( __FILE__, 'is_file' );

		$this->assertTrue( $val->isValid() );
	}



	public function testIsString()
	{
		$val = new Validator( 'a string', 'is_string' );

		$this->assertTrue( $val->isValid() );

		$val->set( '' );

		$this->assertTrue( $val->isValid() );

		$val->set( 100 );

		$this->assertFalse( $val->isValid() );
	}



	public function testIsNonEmptyString()
	{
		$val = new Validator( 'a string', 'is_string minlength|1' );

		$this->assertTrue( $val->isValid() );

		$val->set( '' );

		$this->assertFalse( $val->isValid() );

		$val->set( 100 );

		$this->assertFalse( $val->isValid() );
	}
}